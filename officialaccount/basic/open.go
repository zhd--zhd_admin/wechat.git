package basic

import (
	"fmt"
	"gitee.com/zhd--zhd_admin/wechat/v2/util"
)

var (
	// 查询绑定的开放平台账号
	// 文档：https://developers.weixin.qq.com/doc/oplatform/openApi/OpenApiDoc/miniprogram-management/basic-info-management/getBindOpenAccount.html
	openHave = "https://api.weixin.qq.com/cgi-bin/open/have"
)

// HaveOpenRes 查询绑定的开放平台账号 返回结果
type HaveOpenRes struct {
	util.CommonError
	HaveOpen bool `json:"have_open"`
}

func (basic *Basic) OpenHave() (*HaveOpenRes, error) {
	ak, err := basic.GetAccessToken()
	if err != nil {
		return nil, err
	}
	url := fmt.Sprintf("%s?access_token=%s", openHave, ak)
	data, err := util.HTTPGet(url)
	if err != nil {
		return nil, err
	}
	haveOpenRes := &HaveOpenRes{}
	err = util.DecodeWithError(data, haveOpenRes, "OpenHave")
	return haveOpenRes, err
}
