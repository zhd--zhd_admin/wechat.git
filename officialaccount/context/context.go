package context

import (
	"gitee.com/zhd--zhd_admin/wechat/v2/credential"
	"gitee.com/zhd--zhd_admin/wechat/v2/officialaccount/config"
)

// Context struct
type Context struct {
	*config.Config
	credential.AccessTokenHandle
}
