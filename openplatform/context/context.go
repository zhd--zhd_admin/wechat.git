package context

import (
	"gitee.com/zhd--zhd_admin/wechat/v2/openplatform/config"
)

// Context struct
type Context struct {
	*config.Config
}
